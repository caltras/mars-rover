const mocha = require("mocha");
const Rover = require("../src/model/rover.model");
const Point = require("../src/model/point.model");
var assert = require('assert');

describe("Test for Rover Model", ()=>{

	it("turn to left", ()=>{
		let rover = new Rover("test");
		rover.setPoint(new Point(1,1,"N"));
		rover.turnToLeft();
		assert.equal(rover.getPoint().getDirection(),"W");
		rover.turnToLeft();
		assert.equal(rover.getPoint().getDirection(),"S");
		rover.turnToLeft();
		assert.equal(rover.getPoint().getDirection(),"E");
		rover.turnToLeft();
		assert.equal(rover.getPoint().getDirection(),"N"); 
	});

	it("turn to right", ()=>{
		let rover = new Rover("test");
		rover.setPoint(new Point(1,1,"N"));
		rover.turnToRight();
		assert.equal(rover.getPoint().getDirection(),"E");
		rover.turnToRight();
		assert.equal(rover.getPoint().getDirection(),"S");
		rover.turnToRight();
		assert.equal(rover.getPoint().getDirection(),"W");
		rover.turnToRight();
		assert.equal(rover.getPoint().getDirection(),"N"); 
	});

	it("move the rover 0,1 to 2,2 (no instructions)", ()=>{
		let rover = new Rover("test");
		rover.setPoint(new Point(0,1,"N"));
		rover.turnToRight();
		rover.move();
		rover.move();
		rover.turnToLeft();
		rover.move();

		assert.equal(rover.getPoint().getX(), 2);
		assert.equal(rover.getPoint().getY(), 2);
	});
	it("move the rover 0,1 to 2,4 (with instructions)", ()=>{
		let rover = new Rover("test");
		rover.setPoint(new Point(0,1,"N"));
		rover.setInstructions("RMMLMMM");
		rover.execute();

		assert.equal(rover.getPoint().getX(), 2);
		assert.equal(rover.getPoint().getY(), 4);
	});
});