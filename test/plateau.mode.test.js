const Rover = require("../src/model/rover.model");
const Point = require("../src/model/point.model");
const Plateau = require("../src/model/plateau.model");
var assert = require('assert');

describe("Test for Plateau Model", ()=>{

	it("add rover on plateau", ()=>{
		let rover = new Rover("Test");
		rover.setPoint(new Point(0,0,"N"));
		let plateau = new Plateau(5,5);
		plateau.addRover(rover);

		assert.equal(plateau.getRover(0,0), rover);
	});

	it("move rover over plateau 0,0 to 4,4 (extremities)", ()=>{
		let rover = new Rover("Test");
		rover.setPoint(new Point(0,0,"N"));

		let plateau = new Plateau(5,5);
		plateau.addRover(rover);
		assert.equal(plateau.getRover(0,0), rover);
		rover.setInstructions("RMMMMMLMMMM");
		rover.execute(plateau);
		assert.equal(plateau.getRover(4,4), rover);
	});

});