const Rover = require("../src/model/rover.model");
const Point = require("../src/model/point.model");
const Plateau = require("../src/model/plateau.model");
var Application = require("../src/app");
var assert = require("assert");

describe("Main Application",()=>{

	it("response", ()=>{
		let rover1 = new Rover("Rover1");
		rover1.setPoint(new Point(1,2,"N"));
		rover1.setInstructions("LMLMLMLMM");
		rover1.execute();

		let rover2 = new Rover("Rover2");
		rover2.setPoint(new Point(3,3,"E"));
		rover2.setInstructions("MMRMMRMRRM");
		rover2.execute();

		let app = new Application();
		app.rovers =[rover1,rover2];
		let response = app.printRoversPositions();
		let expected_reponse = "Rover1: 1 3 N\nRover2: 5 1 E\n";
		assert.equal(response,expected_reponse);
	});
});