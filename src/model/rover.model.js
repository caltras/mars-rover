class Rover{

	constructor(_name){
		this.name = _name;
		this.instructions = [];
		this.nextInstruction;
		this.posNextInstruction=0;
		this.point;
	}
	getName(){
		return this.name;
	}
	getPoint(){
		return this.point;
	}
	setPoint(p){
		this.point = p;
	}
	setInstructions(i){
		this.instructions = i;
	}
	getNextInstruction(){
		if(this.posNextInstruction < this.instructions.length){
			let instruction = this.instructions[this.posNextInstruction];
			this.posNextInstruction++;
			return instruction;
		}else{
			return null;
		}
	}

	execute(p){
		let self = this;
		let commands = {
			"L": "turnToLeft",
			"R": "turnToRight",
			"M": "move"
		}
		let inst = self.getNextInstruction();

		while(inst !=null){
			if(p){
				p.clearRover(self.getPoint().getX(), self.getPoint().getY());
			}
			self[commands[inst]]();
			inst = this.getNextInstruction();
		}
		if(p){
			p.addRover(this);
		}
	}
	turnToLeft(){
		let newDirection = {
			"N": "W",
			"S": "E",
			"W": "S",
			"E": "N"
		};
		this.getPoint().setDirection(newDirection[this.getPoint().getDirection()]);
	}
	turnToRight(){
		let newDirection = {
			"N": "E",
			"S": "W",
			"W": "N",
			"E": "S"
		};
		this.getPoint().setDirection(newDirection[this.getPoint().getDirection()]);
	}
	move(){
		let y = this.getPoint().getY();
		let x= this.getPoint().getX();
		switch(this.getPoint().getDirection()){
			case "N":
				this.getPoint().setY(++y);
				break;
			case "S":
				this.getPoint().setY(--y);
				break;
			case "W":
				this.getPoint().setX(--x);
				break;
			case "E":
				this.getPoint().setX(++x);
				break;
		}		
	}
	toString(){
		return this.name + ": "+this.point
	}
}
module.exports = Rover;