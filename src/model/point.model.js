class Point {
	constructor(x, y, direction){
		this.x = x;
		this.y = y;
		this.direction = direction;
	}
	getX(){
		return this.x;
	}
	getY(){
		return this.y;
	}
	setX(_x){
		this.x = _x;
	}
	setY(_y){
		this.y = _y;
	}
	getDirection(){
		return this.direction;
	}
	setDirection(d){
		this.direction = d;
	}
	toString(){
		return "[x:"+this.x+", y:"+this.y+", direction: "+this.direction+"]";
	}
}
module.exports = Point;