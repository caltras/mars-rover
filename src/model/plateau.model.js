let _ = require("lodash");

class Plateau{
	
	constructor(width, height){
		this.width = width;
		this.height = height;
		this.rovers = [];
		this.initialize();
	}
	initialize(){
		this.rovers = new Array();

		for(var i=0;i<this.width;i++){
			this.rovers.push(new Array());
			for(var j=0;j<this.height;j++){
				this.rovers[i][j] = null;
			}
		}
	}
	getRover(x, y){
		let r;
		if(x < this.width && y < this.height){
			r = this.rovers[y][x];
		}
		return r;
	}
	addRover(r){
		if(r.point.getX() < this.width && r.point.getY() < this.height){
			this.rovers[r.point.getY()][r.point.getX()] = r;
		}else{
			if(r.point.getX() >= this.width){
				r.point.setX(this.width-1);
			}
			if(r.point.getY() >= this.height){
				r.point.setY(this.height-1);
			}
		}
		this.rovers[r.point.getY()][r.point.getX()] = r;
	}
	clearRover(x,y){
		if(x < this.width && y < this.height){
			this.rovers[y][x] = null;
		}
	}
	toString(){
		let str = "";
		for(let i=this.width-1;i>=0;i--){
			for(let j=0;j<this.height;j++){
				let rover = this.rovers[i][j];
				if(rover){
					str+=" ("+j+","+i+") "+rover.getName();
				}else{
					str+=" ("+j+","+i+") ";
				}
				str+="|";
			}
			str+="\n";
		}
		return str;
	}


}
module.exports = Plateau;