
let Rover = require("./model/rover.model.js");
let Point = require("./model/point.model.js");
let Plateau = require("./model/plateau.model.js");
let input = require("readline");
let _ = require("lodash");

const readline = require('readline');

const Input = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

class Application{
	constructor(){
		this.currentQuestion = 0;
		this.currentRover = null;
		this.questions = [
			"What's the name to the Rover? ",
			"What's the coordinates to the rover (x:number,y:number,direction[N,S,E,W]:string)? ",
			"Please, send the navigation commands to the rover [L=left, R=right, M=move] ",
			"Continue program ? [y, n] "];

		this.rovers = [];
		this.plateau;
	}
	run(){
		console.log('Welcome to Mars mission.');
		console.log('Now you will control the rovers.');
		let self = this;
		let invalidValue=false;
		do{
			Input.question("What are the upper-right coordinates to Plateau? ", (answer) =>{
				if(answer){
					let coordinates = answer.split(" ");
					let x = Number(coordinates[0]);
					let y = Number(coordinates[1]);
					this.plateau = new Plateau(x+1,y+1);
					invalidValue=false;
					self.exec(self.questions[self.currentQuestion]);
				}else{
					invalidValue=true;
					Input.question("Invalid value. Try again");
				}
			});
		}while(invalidValue);
	}
	processInput(answer){
		let self = this;
		if(answer){
			switch (self.currentQuestion){
				case 0:
					let exists = !!_.find(self.rovers,  (r) =>{
						return r.name.toLowerCase() === answer.toLowerCase;
					});
					if(!exists){
						self.currentRover = new Rover(answer)
						self.rovers.push(self.currentRover);
						self.currentQuestion++;						
					}else{
						console.log("There is a rover with this name. Try again");
					}
					self.exec(self.questions[self.currentQuestion]);
					break;
				case 1:
					self.processPosition(answer).then(()=>{
						self.plateau.addRover(self.currentRover);
						self.exec(self.questions[++self.currentQuestion]);
					}).catch( (e) =>{
						console.log(e);
						console.log("Incorrect values. Try again");
						self.exec(self.questions[self.currentQuestion]);
					})
					break;
				case 2:
					self.processInstructions(answer).then(()=>{
						self.exec(self.questions[++self.currentQuestion]);
					}).catch( (e) => {
						console.log(e);
						console.log("Incorrect values. Try again");
						self.exec(self.questions[self.currentQuestion]);	
					});
					break;
				case 3:
					if(answer==="y"){
						self.currentQuestion = 0;
						self.currentRover=null;
						self.exec(self.questions[self.currentQuestion]);
					}else{
						console.log("Bye Bye");
						console.log(self.plateau.toString());
						console.log("\n");
						console.log(self.printRoversPositions());
					}
					break;
				default:
					console.log("Bye Bye");
					Input.close();
					break;

			}
		}else{
			self.exec("Please, fill the input correctly ");
		}
	}
	printRoversPositions(){
		let str = "";
		_.each(this.rovers,(r)=>{
			let p = r.getPoint();
			str +=r.getName()+": "+p.getX()+" "+p.getY()+" "+p.getDirection()+"\n";	
		});
		Input.close();
		return str;
	}
	processPosition(answer){
		let self = this;
		return new Promise((resolve, reject)=>{
			try{
				let values = answer.split(" ");
				if(values.length == 3){
					let x = Number(values[0].trim()) ;
					let y = Number(values[1].trim());
					if(/^(N|S|W|E)$/.test(values[2].toUpperCase())){
						let direction = values[2].trim().toUpperCase();
						self.currentRover.setPoint(new Point(x,y,direction));
						resolve();
					}else{
						reject();
					}
				}else{
					reject();
				}
			}catch(e){
				reject(e);
			}

		})
	}
	processInstructions(answer){
		let self = this;
		return new Promise((resolve, reject)=>{
			try{
				if(/^(L|M|R)+$/.test(answer.toUpperCase())){
					let instructions = answer.toUpperCase().split("");
					self.currentRover.setInstructions(instructions);
					self.currentRover.execute(self.plateau);
					
					resolve();
				}else{
					reject();
				}
			}catch(e){
				reject(e);
			}

		});
	}
	exec(q){
		let self = this;
		Input.question(q, (answer) => {
			self.processInput(answer);
		});
	}

}
module.exports = Application;